﻿Class MainWindow
    Dim clsTaskbar As New clsTaskbar

    Private Sub MainWindow_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Closed
        clsTaskbar.Show()
    End Sub

    Private Sub MainWindow_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        clsTaskbar.Hide()
    End Sub
End Class
