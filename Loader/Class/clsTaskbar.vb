﻿Imports System
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Diagnostics

Public Class clsTaskbar

    <DllImport("user32.dll")> _
    Private Shared Function GetWindowText(ByVal hWnd As IntPtr, ByVal text As StringBuilder, ByVal count As Integer) As Integer
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Auto)> _
    Private Shared Function EnumThreadWindows(ByVal threadId As Integer, ByVal pfnEnum As EnumThreadProc, ByVal lParam As IntPtr) As Boolean
    End Function
    <DllImport("user32.dll", SetLastError:=True)> _
    Private Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As System.IntPtr
    End Function
    <DllImport("user32.dll", SetLastError:=True)> _
    Private Shared Function FindWindowEx(ByVal parentHandle As IntPtr, ByVal childAfter As IntPtr, ByVal className As String, ByVal windowTitle As String) As IntPtr
    End Function
    <DllImport("user32.dll")> _
    Private Shared Function FindWindowEx(ByVal parentHwnd As IntPtr, ByVal childAfterHwnd As IntPtr, ByVal className As IntPtr, ByVal windowText As String) As IntPtr
    End Function
    <DllImport("user32.dll")> _
    Private Shared Function ShowWindow(ByVal hwnd As IntPtr, ByVal nCmdShow As Integer) As Integer
    End Function
    <DllImport("user32.dll")> _
    Private Shared Function GetWindowThreadProcessId(ByVal hwnd As IntPtr, ByRef lpdwProcessId As Integer) As UInteger
    End Function

    Private Const SW_HIDE As Integer = 0
    Private Const SW_SHOW As Integer = 5

    Private Const VistaStartMenuCaption As String = "Start"
    Private Shared vistaStartMenuWnd As IntPtr = IntPtr.Zero
    Private Delegate Function EnumThreadProc(ByVal hwnd As IntPtr, ByVal lParam As IntPtr) As Boolean

    Public Shared Sub Show()
        SetVisibility(True)
    End Sub

    Public Shared Sub Hide()
        SetVisibility(False)
    End Sub

    Public Shared WriteOnly Property Visible() As Boolean
        Set(ByVal value As Boolean)
            SetVisibility(value)
        End Set
    End Property

    Private Shared Sub SetVisibility(ByVal show As Boolean)
        ' get taskbar window
        Dim taskBarWnd As IntPtr = FindWindow("Shell_TrayWnd", Nothing)

        ' try it the WinXP way first...
        Dim startWnd As IntPtr = FindWindowEx(taskBarWnd, IntPtr.Zero, "Button", "Start")

        If startWnd = IntPtr.Zero Then
            ' try an alternate way, as mentioned on CodeProject by Earl Waylon Flinn
            startWnd = FindWindowEx(IntPtr.Zero, IntPtr.Zero, CType(49175, IntPtr), "Start")
        End If

        If startWnd = IntPtr.Zero Then
            ' ok, let's try the Vista easy way...
            startWnd = FindWindow("Button", Nothing)

            If startWnd = IntPtr.Zero Then
                ' no chance, we need to to it the hard way...
                startWnd = GetVistaStartMenuWnd(taskBarWnd)
            End If
        End If

        ShowWindow(taskBarWnd, If(show, SW_SHOW, SW_HIDE))
        ShowWindow(startWnd, If(show, SW_SHOW, SW_HIDE))
    End Sub

    Private Shared Function GetVistaStartMenuWnd(ByVal taskBarWnd As IntPtr) As IntPtr
        ' get process that owns the taskbar window
        Dim procId As Integer
        GetWindowThreadProcessId(taskBarWnd, procId)

        Dim p As Process = Process.GetProcessById(procId)
        If p IsNot Nothing Then
            ' enumerate all threads of that process...
            For Each t As ProcessThread In p.Threads
                'EnumThreadWindows(t.Id, MyEnumThreadWindowsProc, IntPtr.Zero)
            Next
        End If
        Return vistaStartMenuWnd
    End Function

    Private Shared Function MyEnumThreadWindowsProc(ByVal hWnd As IntPtr, ByVal lParam As IntPtr) As Boolean
        Dim buffer As New StringBuilder(256)
        If GetWindowText(hWnd, buffer, buffer.Capacity) > 0 Then
            Console.WriteLine(buffer)
            If buffer.ToString() = VistaStartMenuCaption Then
                vistaStartMenuWnd = hWnd
                Return False
            End If
        End If
        Return True
    End Function





End Class
